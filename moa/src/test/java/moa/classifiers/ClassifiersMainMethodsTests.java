package moa.classifiers;

import junit.framework.TestSuite;
import junit.framework.Test;

public class ClassifiersMainMethodsTests extends TestSuite {

    public static Test suite() {
        TestSuite suite = new TestSuite();

        suite.addTest(moa.classifiers.bayes.NaiveBayesTest.suite());
        suite.addTest(moa.classifiers.bayes.NaiveBayesMultinomialTest.suite());
        suite.addTest(moa.classifiers.demo.DemoNewFunctionTest.suite());

        return suite;
    }

    public static void main(String []args) {
        junit.textui.TestRunner.run(suite());
    }
}