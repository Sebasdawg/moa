package moa.classifiers.demo;

import junit.framework.TestSuite;
import junit.framework.Test;
import junit.framework.TestCase;

public class DemoNewFunctionTest extends TestCase {
    
    public DemoNewFunctionTest(String name) {
        super(name);
    }

    public void testDemoNewFunction() {
        DemoNewFunction demo = new DemoNewFunction();
        assertEquals(demo.thisIsADemoFunction(), "Demo Function!");
    }

    public static Test suite() {
        return new TestSuite(DemoNewFunctionTest.class);
    }

    public static void main(String []args) {
        junit.textui.TestRunner.run(suite());
    }
}
